#include "ArduinoReader.h"
#include <QDebug>
#include <QTextStream>

const quint16 T_ADC_PLACEHOLDER = 0x400;
const double T_CDEG_PLACEHOLDER = -273;

ArduinoReader::ArduinoReader(QObject* parent) :
    QObject(parent),
    serial("/dev/ttyUSB0", this)
{
    // serial.setPortName("/dev/ttyUSB0");
    serial.setBaudRate(QSerialPort::Baud9600);
    
    connect(&serial, &QSerialPort::readyRead, this, &ArduinoReader::handleReadyRead);

    serial.open(QIODevice::ReadOnly);
}

ArduinoReader::~ArduinoReader()
{
    serial.close();
}

void ArduinoReader::handleReadyRead()
{
    arduinoData.append( serial.readAll() );
    
    quint16 t_adc = T_ADC_PLACEHOLDER;
    double t_cdeg = T_CDEG_PLACEHOLDER;

    int pos;
    // qInfo() << "arduinoData: " << arduinoData << endl;
    while ((pos = arduinoData.indexOf("\n")) != -1)
    {
        t_adc = T_ADC_PLACEHOLDER;
        t_cdeg = T_CDEG_PLACEHOLDER;
        int idx = 0;

        QByteArray row = arduinoData.left(pos);
        // qInfo() << QString("ArduinoReader: {%1}").arg((QString) row) << endl;
        for (QByteArray token : row.split(' '))
        {
            switch (idx++)
            {
                case 0: t_adc = token.toInt(); break;
                case 1: t_cdeg = token.toDouble(); 
            }
        }
        
        arduinoData.remove(0, pos + 1);
    }

    if (t_adc != T_ADC_PLACEHOLDER && t_cdeg != T_CDEG_PLACEHOLDER) 
    {
        // qInfo() << QString("ArduinoReader: [%1, %2]").arg(t_adc).arg(t_cdeg) << endl;
        emit done(t_adc, t_cdeg);
    }
}