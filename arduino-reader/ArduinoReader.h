#ifndef ARDUINO_READER
#define ARDUINO_READER

#include <QSerialPort>

class ArduinoReader : public QObject 
{
    Q_OBJECT

public:
    ArduinoReader(QObject* = nullptr);
    ~ArduinoReader();

private slots:
    void handleReadyRead();

signals:
    void done(quint16 t_adc, double t_cdeg);

private:
    QSerialPort serial;
    QByteArray  arduinoData;
};

#endif